package com.cie.repositorio.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCarpetaPermiso is a Querydsl query type for CarpetaPermiso
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCarpetaPermiso extends EntityPathBase<CarpetaPermiso> {

    private static final long serialVersionUID = -352572875L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCarpetaPermiso carpetaPermiso = new QCarpetaPermiso("carpetaPermiso");

    public final QCarpeta carpeta;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> permiso = createNumber("permiso", Integer.class);

    public final QUsuario usuario;

    public QCarpetaPermiso(String variable) {
        this(CarpetaPermiso.class, forVariable(variable), INITS);
    }

    public QCarpetaPermiso(Path<? extends CarpetaPermiso> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCarpetaPermiso(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCarpetaPermiso(PathMetadata metadata, PathInits inits) {
        this(CarpetaPermiso.class, metadata, inits);
    }

    public QCarpetaPermiso(Class<? extends CarpetaPermiso> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.carpeta = inits.isInitialized("carpeta") ? new QCarpeta(forProperty("carpeta")) : null;
        this.usuario = inits.isInitialized("usuario") ? new QUsuario(forProperty("usuario"), inits.get("usuario")) : null;
    }

}

