package com.cie.repositorio.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCarpeta is a Querydsl query type for Carpeta
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCarpeta extends EntityPathBase<Carpeta> {

    private static final long serialVersionUID = 341116224L;

    public static final QCarpeta carpeta = new QCarpeta("carpeta");

    public final ArrayPath<byte[], Byte> archivo = createArray("archivo", byte[].class);

    public final StringPath descripcion = createString("descripcion");

    public final BooleanPath directory = createBoolean("directory");

    public final BooleanPath enable = createBoolean("enable");

    public final StringPath extension = createString("extension");

    public final DateTimePath<java.util.Date> fechaActualizacion = createDateTime("fechaActualizacion", java.util.Date.class);

    public final DateTimePath<java.util.Date> fechaCreacion = createDateTime("fechaCreacion", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> idParent = createNumber("idParent", Long.class);

    public final StringPath nombre = createString("nombre");

    public final StringPath nombreArchivo = createString("nombreArchivo");

    public final StringPath usuarioActualizacion = createString("usuarioActualizacion");

    public final StringPath usuarioCreacion = createString("usuarioCreacion");

    public QCarpeta(String variable) {
        super(Carpeta.class, forVariable(variable));
    }

    public QCarpeta(Path<? extends Carpeta> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCarpeta(PathMetadata metadata) {
        super(Carpeta.class, metadata);
    }

}

