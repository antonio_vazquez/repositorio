package com.cie.repositorio.repository;

import com.cie.repositorio.model.db.Rol;
import com.cie.repositorio.model.db.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface RolRepository extends JpaRepository<Rol, Long>, QuerydslPredicateExecutor<Rol> {


}
