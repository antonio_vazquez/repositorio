package com.cie.repositorio.repository;

import com.cie.repositorio.model.db.CarpetaPermiso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface CarpetaPermisoRepository extends JpaRepository<CarpetaPermiso, Long>, QuerydslPredicateExecutor<CarpetaPermiso> {

    List<CarpetaPermiso> findAllByUsuarioId(long usuarioId);

    List<CarpetaPermiso> findAllByCarpetaId(long carpetaId);

    CarpetaPermiso findAllByUsuarioIdAndCarpetaId(long usuarioId, long carpetaId);

}
