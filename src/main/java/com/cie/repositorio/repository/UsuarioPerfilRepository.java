package com.cie.repositorio.repository;

import com.cie.repositorio.model.db.UsuarioPerfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface UsuarioPerfilRepository extends JpaRepository<UsuarioPerfil, Long>, QuerydslPredicateExecutor<UsuarioPerfil> {

}
