package com.cie.repositorio.repository;

import com.cie.repositorio.model.db.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>, QuerydslPredicateExecutor<Usuario> {
    

    Usuario findByCorreo(String correo);

}
