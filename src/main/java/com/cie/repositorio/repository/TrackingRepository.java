package com.cie.repositorio.repository;

import com.cie.repositorio.model.db.Rol;
import com.cie.repositorio.model.db.Tracking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface TrackingRepository extends JpaRepository<Tracking, Long>, QuerydslPredicateExecutor<Tracking> {


}
