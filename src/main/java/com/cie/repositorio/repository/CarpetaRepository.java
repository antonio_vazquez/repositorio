package com.cie.repositorio.repository;

import com.cie.repositorio.model.db.Carpeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface CarpetaRepository extends JpaRepository<Carpeta, Long>, QuerydslPredicateExecutor<Carpeta> {

    List<Carpeta> findAllByIdParent(Long idParent);

}
