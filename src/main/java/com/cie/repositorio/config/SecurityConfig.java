package com.cie.repositorio.config;

import com.cie.repositorio.model.db.Usuario;
import com.cie.repositorio.services.db.UsuarioService;
import com.cie.repositorio.services.logic.UserDetailServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public final Integer SESSION_TIMEOUT_IN_SECONDS = 60 * 30;

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityConfig.class);

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(NoOpPasswordEncoder.getInstance());
    }

    @Override
    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailServiceImpl();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String[] resources = new String[]{
                "/css/**","/images/**","/js/**","/options/**","/plugins/**", "/assets/**", "/ajax-load/**",
                "/color-switcher/**","/fonts/**","/includes/**","/video/**","/scripts/**"
        };
        http.headers().cacheControl().disable()
                .and()
                    .authorizeRequests()
                    .antMatchers("/advisor/**").hasAnyAuthority("IT","ADMIN","DIRECCION")
                    .antMatchers("/common/**").hasAnyAuthority("IT","ADMIN","DIRECCION")
                    .antMatchers("/rol/**").hasAnyAuthority("IT","ADMIN")
                    .antMatchers("/usuario/**").hasAnyAuthority("IT","ADMIN")
                    .antMatchers("/usuarioperfil/**").hasAnyAuthority("IT","ADMIN")
                    .antMatchers("/permiso/**").hasAnyAuthority("IT","ADMIN")
                    .antMatchers("/carpeta/**").hasAnyAuthority("IT","ADMIN","DIRECCION")
                    .antMatchers("/tracking/**").hasAnyAuthority("IT","ADMIN")

                .and()
                    .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/rfq")
                    .failureUrl("/login?error")
                    .successHandler(successHandler())
                    .usernameParameter("username").passwordParameter("password")
                    .failureHandler(failureHandler())
                    .permitAll()
                .and()
                    .logout().permitAll()
                .and()
                    .csrf().disable();
    }

    @Bean
    AuthenticationSuccessHandler successHandler() {
        return (request, response, authentication) -> {
            //request.getSession().setMaxInactiveInterval(SESSION_TIMEOUT_IN_SECONDS);
            Usuario usuario = usuarioService.findByCorreo(authentication.getName());
            request.getSession().setMaxInactiveInterval(-1);
            response.sendRedirect(request.getContextPath() + "/common/");
        };
    }

    @Bean
    AuthenticationFailureHandler failureHandler() {
        return (request, response, exception) -> {
            request.setAttribute("statusMessage",  "test");
            HttpSession session = request.getSession();
            if (session != null) {
                session.removeAttribute("userDisabled");
                session.removeAttribute("badCredentials");
                session.removeAttribute("requiredCredentials");
            }
            if (exception instanceof DisabledException) {
                session.setAttribute("userDisabled", "Usuario deshabilitado.");
            } else if (exception instanceof BadCredentialsException) {
                session.setAttribute("badCredentials", "Usuario y/o contraseña incorrectos.");
            } else {
                session.setAttribute("requiredCredentials", "Usuario y/o contraseña incorrectos.");
            }
            response.sendRedirect("/login?error");
        };
    }


    private void addMatch(HashMap<String, Set<String>> finish, String url, String nombre) {
        if (!finish.containsKey(url)) {
            finish.put(url, new LinkedHashSet<>());
        }
        Set<String> set = finish.get(url);
        set.add(nombre);
    }

}