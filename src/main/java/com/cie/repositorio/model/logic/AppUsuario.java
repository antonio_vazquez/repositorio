package com.cie.repositorio.model.logic;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class AppUsuario extends org.springframework.security.core.userdetails.User {

    private String nombre;

    private String correo;

    private String foto;

    private Boolean multicompania;

    public AppUsuario(
            String username,
            String password,
            boolean enabled,
            boolean accountNonExpired,
            boolean credentialsNonExpired,
            boolean accountNonLocked,
            Collection<GrantedAuthority> authorities,
            String nombre,
            String foto,
            Boolean multicompania) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, authorities);
        this.correo = username;
        this.nombre = nombre;
        this.foto = foto;
        this.multicompania = multicompania != null ? multicompania : false;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public String getFoto() {
        return foto;
    }

    public Boolean getMulticompania() {
        return multicompania;
    }
}
