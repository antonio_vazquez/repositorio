package com.cie.repositorio.model.logic;

import com.cie.repositorio.model.db.Carpeta;
import com.cie.repositorio.model.db.Usuario;

import javax.persistence.*;

public class CarpetaPermisoWrapper {

    private Long id;

    private Carpeta carpeta;

    private int permiso;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Carpeta getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(Carpeta carpeta) {
        this.carpeta = carpeta;
    }

    public int getPermiso() {
        return permiso;
    }

    public void setPermiso(int permiso) {
        this.permiso = permiso;
    }
}
