package com.cie.repositorio.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUsuarioPerfil is a Querydsl query type for UsuarioPerfil
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUsuarioPerfil extends EntityPathBase<UsuarioPerfil> {

    private static final long serialVersionUID = -1156781212L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUsuarioPerfil usuarioPerfil = new QUsuarioPerfil("usuarioPerfil");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QRol rol;

    public final QUsuario usuario;

    public QUsuarioPerfil(String variable) {
        this(UsuarioPerfil.class, forVariable(variable), INITS);
    }

    public QUsuarioPerfil(Path<? extends UsuarioPerfil> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUsuarioPerfil(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUsuarioPerfil(PathMetadata metadata, PathInits inits) {
        this(UsuarioPerfil.class, metadata, inits);
    }

    public QUsuarioPerfil(Class<? extends UsuarioPerfil> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.rol = inits.isInitialized("rol") ? new QRol(forProperty("rol")) : null;
        this.usuario = inits.isInitialized("usuario") ? new QUsuario(forProperty("usuario"), inits.get("usuario")) : null;
    }

}

