package com.cie.repositorio.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTracking is a Querydsl query type for Tracking
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTracking extends EntityPathBase<Tracking> {

    private static final long serialVersionUID = -1043166291L;

    public static final QTracking tracking = new QTracking("tracking");

    public final StringPath action = createString("action");

    public final BooleanPath directory = createBoolean("directory");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public final DateTimePath<java.util.Date> register = createDateTime("register", java.util.Date.class);

    public final StringPath track = createString("track");

    public final StringPath user = createString("user");

    public QTracking(String variable) {
        super(Tracking.class, forVariable(variable));
    }

    public QTracking(Path<? extends Tracking> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTracking(PathMetadata metadata) {
        super(Tracking.class, metadata);
    }

}

