package com.cie.repositorio.controller;

import com.cie.repositorio.form.NavForm;
import com.cie.repositorio.model.db.Usuario;
import com.cie.repositorio.model.db.UsuarioPerfil;
import com.cie.repositorio.repository.RolRepository;
import com.cie.repositorio.repository.UsuarioPerfilRepository;
import com.cie.repositorio.repository.UsuarioRepository;
import com.cie.repositorio.services.db.UsuarioPerfilService;
import com.cie.repositorio.services.db.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/usuarioperfil")
public class UsuarioPerfilController {


    @Autowired
    private UsuarioPerfilRepository usuarioPerfilRepository;

    @Autowired
    private UsuarioPerfilService usuarioPerfilService;

    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService  usuarioService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("form1", new NavForm());
        model.addAttribute("entities", usuarioRepository.findAll());
        return "usuarioperfil/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        List<Usuario> usuarioList1 = new ArrayList<>();
        List<Usuario> usuarioList = usuarioRepository.findAll();
        for (Usuario  u: usuarioList){
            if (u.getRol() == null){
                usuarioList1.add(u);
            }
        }

        model.addAttribute("form1", new NavForm());
        model.addAttribute("form", new UsuarioPerfil());
        model.addAttribute("usuario", usuarioList1);
        model.addAttribute("rol", rolRepository.findAll() );
       return "usuarioperfil/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Usuario usuario, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", new UsuarioPerfil());
            model.addAttribute("usuario", usuarioRepository.findAll());
            model.addAttribute("rol",rolRepository.findAll());
            return "usuarioperfil/create";
        }

//        UsuarioPerfil insert = usuarioPerfilRepository.save(usuarioperfil);

        Usuario insert = usuarioService.updateProfile(usuario);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Perfil registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/usuarioperfil";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {

        List<Usuario> usuarioList1 = new ArrayList<>();
        List<Usuario> usuarioList = usuarioRepository.findAll();
        for (Usuario  u: usuarioList){
            if (u.getRol() == null){
                usuarioList1.add(u);
            }
        }
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", usuarioRepository.findById(id));
            model.addAttribute("rol", rolRepository.findAll());
            return "usuarioperfil/update";
    }


    @PostMapping("/update")
    public String usuarioPerfilUpdate(Model model, @Validated @ModelAttribute("form") Usuario form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "usuarioperfil/update";
        }
        Usuario update = usuarioService.updateProfile(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/usuarioperfil";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        usuarioPerfilService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Registro eliminado");
        return "redirect:/usuarioperfil";
    }




}
