package com.cie.repositorio.controller;

import com.cie.repositorio.form.NavForm;
import com.cie.repositorio.form.TrackingForm;
import com.cie.repositorio.model.db.Carpeta;
import com.cie.repositorio.model.db.Tracking;
import com.cie.repositorio.repository.TrackingRepository;
import com.cie.repositorio.services.db.TrackingService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/tracking")
public class TrackingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrackingController.class);

    @Autowired
    private TrackingRepository trackingRepository;

    @Autowired
    private TrackingService trackingService;

    @RequestMapping
    public String index(Model model) throws Exception {
        List<Tracking> trackingList = trackingService.findAll();
        Gson js = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create()
                ;
        for (Tracking t : trackingList){
            try {
                t.setCarpeta(js.fromJson(t.getTrack(), Carpeta.class));
            } catch (Exception e) {
                LOGGER.error("ID[" + t.getId() + "]");
                LOGGER.error("ID[" + ExceptionUtils.getFullStackTrace(e) + "]");
            }
        }
        /*model.addAttribute("entities", trackingList);*/
        model.addAttribute("form1", new NavForm());
        model.addAttribute("entities", null);
        model.addAttribute("form", new TrackingForm());
        return "tracking/index";
    }

    @PostMapping
    public String index(Model model, @Validated @ModelAttribute("form") TrackingForm trackingForm, final BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            List<Tracking> trackingList = trackingService.queryBuilder(trackingForm.getDesde(),trackingForm.getHasta(),trackingForm.getNombre(),trackingForm.getUsuario());
            Gson js = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
            for (Tracking t : trackingList){
                t.setCarpeta(js.fromJson(t.getTrack(), Carpeta.class));
            }
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", new TrackingForm());
            model.addAttribute("entities", trackingList);
        }
        return "tracking/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form1", new NavForm());
        model.addAttribute("form", new Tracking());
       return "tracking/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Tracking tracking, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            return "tracking/create";
        }

        Tracking insert = trackingRepository.save(tracking);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Tracking registrado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/tracking";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", trackingRepository.findById(id));
            return "tracking/update";
    }


    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") Tracking form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "tracking/update";
        }
        Tracking update = trackingService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registro actualizado correctamente");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "No se pudo registrar correctamente, por favor intente más tarde");
        }
        return "redirect:/tracking";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        trackingService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Registro eliminado");
        return "redirect:/tracking";
    }




}
