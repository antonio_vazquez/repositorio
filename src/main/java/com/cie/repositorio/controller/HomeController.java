package com.cie.repositorio.controller;

import com.cie.repositorio.form.NavForm;
import com.cie.repositorio.form.TrackingForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/")
public class HomeController {


    @RequestMapping
    public String index(final Principal principal) {
        if (principal != null) {
            return "redirect:/common/";
        }
        return "redirect:login";
    }

    @GetMapping("login")
    public String login(final Principal principal) {
        if (principal != null) {
            return "redirect:/common/";
        }
        return "login";
    }

    @GetMapping("common")
    public String common(Model model) {

        model.addAttribute("form1", new NavForm());
        return "common/index";
    }

}
