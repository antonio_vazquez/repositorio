package com.cie.repositorio.controller;

import com.cie.repositorio.form.NavForm;
import com.cie.repositorio.form.TrackingForm;
import com.cie.repositorio.model.db.Carpeta;
import com.cie.repositorio.model.db.CarpetaPermiso;
import com.cie.repositorio.model.db.Tracking;
import com.cie.repositorio.model.db.Usuario;
import com.cie.repositorio.model.logic.CarpetaForm;
import com.cie.repositorio.repository.CarpetaRepository;
import com.cie.repositorio.services.db.CarpetaPermisoService;
import com.cie.repositorio.services.db.CarpetaService;
import com.cie.repositorio.services.db.TrackingService;
import com.cie.repositorio.services.db.UsuarioService;
import com.cie.repositorio.services.logic.CarpetaManagerService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/carpeta")
public class CarpetaController {

    @Autowired
    private CarpetaService carpetaService;

    @Autowired
    private CarpetaRepository carpetaRepository;

    @Autowired
    private CarpetaManagerService carpetaManagerService;

    @Autowired
    private TrackingService trackingService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private CarpetaPermisoService carpetaPermisoService;

    @RequestMapping("/{parentid}")
    public String index(Model model, @PathVariable("parentid") Long parentId, Principal principal) throws Exception {
        Usuario usuario = usuarioService.findByCorreo(principal.getName());
        List<Carpeta> carpetaList1 = carpetaManagerService.findSons(parentId, usuario.getId()).stream().sorted(Comparator.comparing(Carpeta::isDirectory).reversed().thenComparing(Carpeta::getNombre)).collect(Collectors.toList());
        List<Carpeta> carpetaList = new ArrayList<>();
        for (Carpeta c : carpetaList1){
            if(c.isEnable()){
                carpetaList.add(c);
            }
        }

        model.addAttribute("usuarioId", usuario.getId());
        model.addAttribute("form1", new NavForm());
        model.addAttribute("entities", carpetaList);
        model.addAttribute("parentId", parentId);
        model.addAttribute("add", parentId != 0 ? carpetaPermisoService.findAllByUsuarioAndCarpeta(usuario.getId(), parentId).getPermiso()  : 0);
        model.addAttribute("path", carpetaManagerService.buildPath(parentId));
        return "carpeta/index";
    }

    @RequestMapping("regresar/{parentid}")
    public String indexReturn(Model model, @PathVariable("parentid") Long parentId, Principal principal) throws Exception {
        Usuario usuario = usuarioService.findByCorreo(principal.getName());
       parentId = carpetaService.findOne(parentId).getIdParent();


        List<Carpeta> carpetaList1 = carpetaManagerService.findSons(parentId, usuario.getId()).stream().sorted(Comparator.comparing(Carpeta::isDirectory).reversed().thenComparing(Carpeta::getNombre)).collect(Collectors.toList());
        List<Carpeta> carpetaList = new ArrayList<>();
/*        List<Carpeta> carpetaList = carpetaManagerService.findSons(parentId);
        List<Carpeta> carpetaList2 = carpetaList1.stream().sorted(Comparator.comparing(Carpeta::getFechaCreacion)).collect(Collectors.toList());
        Collections.sort(carpetaList, new Comparator<Carpeta>() {
            @Override
            public int compare(Carpeta o1, Carpeta o2) {
                return o2.getId().compareTo(o1.getId());
            }
        });*/
        for (Carpeta c : carpetaList1){
            if(c.isEnable()){
                carpetaList.add(c);
            }
        }

        model.addAttribute("form1", new NavForm());
//        model.addAttribute("entities", carpetaList1);
        model.addAttribute("entities", carpetaList);
        model.addAttribute("parentId", parentId);
        model.addAttribute("add", carpetaService.findOne(parentId).getPermiso());
        model.addAttribute("path", carpetaManagerService.buildPath(parentId));
        return "carpeta/index";
    }

    @PostMapping
    public String indexFind(Model model, @Validated @ModelAttribute("form1") NavForm navForm, final BindingResult bindingResult, Principal principal) {
        if (!bindingResult.hasErrors()) {
            Usuario usuario = usuarioService.findByCorreo(principal.getName());
            List<Carpeta> carpetaList1 = new ArrayList<>();
            List<Carpeta> carpetaList = carpetaManagerService.findIndex(carpetaService.queryBuilder(navForm.getNombre()),usuario.getId());
            carpetaList.forEach(p-> p.setPath(carpetaManagerService.buildPathToString(p.getIdParent())));
            for (Carpeta c : carpetaList){
                if(c.isEnable()){
                    carpetaList1.add(c);
                }
            }
            model.addAttribute("form1", new NavForm());
//            model.addAttribute("entities", carpetaList);
            model.addAttribute("entities", carpetaList1);
            model.addAttribute("column", true);
        }
        return "carpeta/index";
    }


    @RequestMapping("/create/{parentId}")
    public String create(Model model, @PathVariable Long parentId)  {
        CarpetaForm cf = new CarpetaForm();
        cf.setParentId(parentId);
        model.addAttribute("form1", new NavForm());
        model.addAttribute("form", cf);
       return "carpeta/create";
    }

    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") CarpetaForm carpeta, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            CarpetaForm cf = new CarpetaForm();
            cf.setParentId(carpeta.getParentId());
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", cf);
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "You must add the name of the document");
            return "redirect:/carpeta/create/" + carpeta.getParentId();
        }
        if (carpeta.isDirectorio() != true && carpeta.getArchivo().isEmpty()){
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "The registration was not completed, it is required to add a file");
            return "redirect:/carpeta/create/" + carpeta.getParentId();
        }
        List<Carpeta> carpetaList = carpetaService.findAllByParent(carpeta.getParentId());
        for (Carpeta c : carpetaList){
            if (c.getNombre().toUpperCase().equals(carpeta.getNombre().toUpperCase())){
                redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Duplicate name is not allowed at this level");
                return "redirect:/carpeta/" + carpeta.getParentId();

            }
        }
        if (carpetaManagerService.insertCarpeta(carpeta, bindingResult, principal.getName())) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Registered Successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/carpeta/" + carpeta.getParentId();
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
        if (carpetaRepository.findById(id).get().isDirectory()){
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", carpetaRepository.findById(id));
            return "carpeta/update";
        }else{
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", carpetaRepository.findById(id));
            return "carpeta/updatefile";
        }
    }

    @PostMapping("/update")
    public String intranetUpdate(Model model, @Validated @ModelAttribute("form") CarpetaForm form, final BindingResult bindingResult, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            return "carpeta/update";
        }
        form.setUsuarioActualizacion(principal.getName());
        Carpeta update = carpetaService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/carpeta/" + carpetaService.findOne(form.getId()).getIdParent();
    }

    @PostMapping("/updatefile")
    public String intranetUpdateFile(Model model, @Validated @ModelAttribute("form") CarpetaForm form, final BindingResult bindingResult, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            return "carpeta/updatefile";
        }
        form.setUsuarioActualizacion(principal.getName());
        Carpeta update = carpetaService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }

        return "redirect:/carpeta/" + carpetaService.findOne(form.getId()).getIdParent();
    }

    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes, Principal principal) {
        Long idP = carpetaService.findOne(id).getIdParent();
        Carpeta log = carpetaService.findOne(id);

        Tracking tracking = new Tracking();
        tracking.setAction("DELETE");
        tracking.setRegister(new Date());
        tracking.setUser(principal.getName());
        tracking.setDirectory(log.isDirectory());
        tracking.setNombre(log.getNombre());
        Gson js = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
        tracking.setTrack(js.toJson(log,Carpeta.class));
        trackingService.insert(tracking);
//        carpetaService.delete(id);
        carpetaService.disableFile(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/carpeta/" + idP;
    }

    @RequestMapping(value = "/media/{id}")
    public void report(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
        Carpeta carpeta = carpetaService.findOne(id);
        byte[] report = carpeta.getArchivo();
        response.setContentType(content(carpeta.getExtension()));
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename=" + carpeta.getNombreArchivo());
        OutputStream out = response.getOutputStream();
        out.write(report);
    }

    public String content(String extension) {
        try {
            switch (extension) {
                case "PDF": return "application/pdf";
                case "XLSM": return "application/vnd.ms-excel.sheet.macroEnabled.12";
                case "XLSX": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case "XLSB": return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                case "XLS": return "application/vnd.ms-excel";
                case "DOC": return "application/msword";
                case "DOCX": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case "DOCM": return "application/vnd.ms-word.document.macroEnabled.12";
                case "DOTM": return "application/vnd.ms-word.template.macroEnabled.12";
                case "PPT": return "application/vnd.ms-powerpoint";
                case "PPTM": return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                case "POT": return "application/vnd.ms-powerpoint";
                case "PPTX": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case "JPG": return "image/jpeg";
                case "JPEG": return "image/jpeg";
                case "PNG": return "image/png";
                case "TIF": return "image/tiff";
                case "GIF": return "image/gif";
                case "DWG": return "image/vnd.dwg";
                case "DXF": return "image/vnd.dwg";
                case "VSD": return "application/vnd.visio";
                case "TXT": return "text/plain";
                case "CSV": return "text/csv";
                case "ZIP": return "application/zip";
                case "RAR": return "application/x-rar-compressed";
                case "XML": return "application/xml";
                default: return "application/pdf";
            }
        } catch (Exception e) {
            return "application/pdf";
        }
    }

    @RequestMapping(value = "/move/{carpetaId}/{carpetaDesinityId}")
    public String moveFile(@PathVariable("carpetaId") Long carpetaId, @PathVariable("carpetaDesinityId") Long carpetaDesinityId, RedirectAttributes redirectAttributes, Principal principal) throws IOException {
        Carpeta carpeta = carpetaService.findOne(carpetaId);
        CarpetaPermiso carpetaPermisoFinal = new CarpetaPermiso();
        CarpetaPermiso carpetaPermisoFolder = new CarpetaPermiso();
        List<Carpeta> carpetaList = carpetaService.findAllByParent(carpetaDesinityId);
        for (Carpeta c : carpetaList){
            if (c.getNombre().toUpperCase().equals(carpeta.getNombre().toUpperCase())){
                redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Duplicate name is not allowed at this level");
                return "redirect:/carpeta/" + carpetaDesinityId;

            }
        }
        List<CarpetaPermiso> carpetaPermisoList = carpetaPermisoService.findAllByCarpertaId(carpetaId);
        for (CarpetaPermiso carpetaPermiso :  carpetaPermisoList){
            carpetaPermisoService.delete(carpetaPermiso);
        }
        List<CarpetaPermiso> carpetaPermisoListPadre = carpetaPermisoService.findAllByCarpertaId(carpetaDesinityId);
        List<CarpetaPermiso> carpetaPermisoListFinal = new ArrayList<>();
        int index =0;
        for (CarpetaPermiso c : carpetaPermisoListPadre){
            carpetaPermisoFinal = new CarpetaPermiso();
            carpetaPermisoFinal.setPermiso(c.getPermiso());
            carpetaPermisoFinal.setCarpeta(carpetaService.findOne(carpetaId));
            carpetaPermisoFinal.setUsuario(c.getUsuario());
            carpetaPermisoListFinal.add(index, carpetaPermisoFinal);
            index++;
        }
        for (CarpetaPermiso c2 : carpetaPermisoListFinal){
            carpetaPermisoService.insert(c2);
        }
        if (carpeta.isDirectory()){
            carpetaList = new ArrayList<>();
            carpetaList = carpetaService.findAllByParent(carpetaId);
            for (Carpeta crpt : carpetaList){
                carpetaPermisoService.delete(carpetaPermisoService.findAllByCarpertaId(crpt.getId()));
            }
            carpetaPermisoList = new ArrayList<>();
            List<CarpetaPermiso> carpetaPermisoListFolder = new ArrayList<>();
            carpetaPermisoList = carpetaPermisoService.findAllByCarpertaId(carpetaDesinityId);

            for (Carpeta crpt1 : carpetaList){
                carpetaPermisoListFolder = new ArrayList<>();
                index =0;
                for (CarpetaPermiso crtP : carpetaPermisoList){
                    carpetaPermisoFolder = new CarpetaPermiso();
                    carpetaPermisoFolder.setPermiso(crtP.getPermiso());
                    carpetaPermisoFolder.setCarpeta(carpetaService.findOne(crpt1.getId()));
                    carpetaPermisoFolder.setUsuario(crtP.getUsuario());
                    carpetaPermisoListFolder.add(index, carpetaPermisoFolder);
                    index++;
                }
                for (CarpetaPermiso crtP2 : carpetaPermisoListFolder){
                    carpetaPermisoService.insert(crtP2);
                }

            }


        }





/*        CarpetaPermiso carpetaPermiso = carpetaPermisoService.findAllByUsuarioAndCarpeta(usuarioService.findByCorreo(principal.getName()).getId(),carpetaId);
        CarpetaPermiso carpetaPermisoDestino = carpetaPermisoService.findAllByUsuarioAndCarpeta(usuarioService.findByCorreo(principal.getName()).getId(),carpetaDesinityId);
        carpetaPermiso.setPermiso(carpetaPermiso.getPermiso());
        carpetaPermisoService.update(carpetaPermiso);*/
        carpeta.setIdParent(carpetaDesinityId);
        carpetaService.update(carpeta);
        return "redirect:/carpeta/" + carpetaDesinityId;
    }

}
