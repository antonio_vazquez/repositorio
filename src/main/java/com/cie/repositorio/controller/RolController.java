package com.cie.repositorio.controller;

import com.cie.repositorio.form.NavForm;
import com.cie.repositorio.model.db.Rol;
import com.cie.repositorio.repository.RolRepository;
import com.cie.repositorio.services.db.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/rol")
public class RolController {



    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private RolService rolService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("form1", new NavForm());
        model.addAttribute("entities", rolRepository.findAll());
        return "rol/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form1", new NavForm());
        model.addAttribute("form", new Rol());
       return "rol/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Rol rol, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            return "rol/create";
        }

        Rol insert = rolRepository.save(rol);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Successfully registered");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/rol";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", rolRepository.findById(id));
            return "rol/update";
    }


    @PostMapping("/update")
    public String rolUpdate(Model model, @Validated @ModelAttribute("form") Rol form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "rol/update";
        }
        Rol update = rolService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/rol";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        rolService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/rol";
    }




}
