package com.cie.repositorio.controller;

import com.cie.repositorio.form.NavForm;
import com.cie.repositorio.model.db.Usuario;
import com.cie.repositorio.repository.RolRepository;
import com.cie.repositorio.repository.UsuarioRepository;
import com.cie.repositorio.services.db.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {


    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private RolRepository rolRepository;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("form1", new NavForm());
        model.addAttribute("entities", usuarioService.findAll());
        return "usuario/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form1", new NavForm());
        model.addAttribute("form", new Usuario());
        model.addAttribute("rol", rolRepository.findAll() );
       return "usuario/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Usuario usuario, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", new Usuario());
            model.addAttribute("rol",rolRepository.findAll());
            return "usuario/create";
        }

        Usuario insert = usuarioRepository.save(usuario);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "User successfully registered");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/usuario";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form1", new NavForm());
            model.addAttribute("form", usuarioRepository.findById(id));
            model.addAttribute("rol", rolRepository.findAll());
            return "usuario/update";
    }


    @PostMapping("/update")
    public String usuarioUpdate(Model model, @Validated @ModelAttribute("form") Usuario form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "usuario/update";
        }
        Usuario update = usuarioService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/usuario";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        usuarioService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/usuario";
    }




}
