package com.cie.repositorio.controller;

import com.cie.repositorio.form.NavForm;
import com.cie.repositorio.model.db.Carpeta;
import com.cie.repositorio.model.db.CarpetaPermiso;
import com.cie.repositorio.model.logic.CarpetaPermisoWrapper;
import com.cie.repositorio.model.logic.CarpetaWrapper;
import com.cie.repositorio.services.db.CarpetaPermisoService;
import com.cie.repositorio.services.db.CarpetaService;
import com.cie.repositorio.services.db.UsuarioService;
import com.cie.repositorio.services.logic.CarpetaPermisoManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.cie.repositorio.controller.PermisoController.carpetaIndex;
import static com.cie.repositorio.controller.PermisoController.carpetaPermisoIndex;

@RestController
@RequestMapping("/permisorest")
public class PermisoRestController {

    @Autowired
    private CarpetaPermisoManagerService carpetaPermisoManagerService;

    @Autowired
    private CarpetaService carpetaService;

    @Autowired
    private CarpetaPermisoService carpetaPermisoService;

    @RequestMapping("/save")
    public List<CarpetaPermiso> save(@RequestBody List<CarpetaPermiso> carpetaPermisoList) throws Exception {
        carpetaPermisoManagerService.insertCarpeta(carpetaPermisoList);
        return null;
    }

    @RequestMapping("/carpeta/{usuarioId}")
    public List<CarpetaWrapper> carpetaUsuario(Model model, @PathVariable("usuarioId") Long usuarioId) throws Exception {
        List<Carpeta> carpetaList = carpetaService.findAll().stream().filter(p->p.isEnable()).collect(Collectors.toList());
        List<CarpetaPermiso> carpetaPermisoList = carpetaPermisoService.findAllByUsuario(usuarioId);
        List<CarpetaPermisoWrapper> cpwl = new ArrayList<>();
        if (carpetaPermisoList != null && carpetaPermisoList.size() > 0) {
            cpwl = carpetaPermisoList.stream().map(carpetaPermisoIndex).collect(Collectors.<CarpetaPermisoWrapper>toList());
        }
        model.addAttribute("permiso", cpwl);
        return carpetaList.stream().map(carpetaIndex).collect(Collectors.<CarpetaWrapper>toList());
    }

    @RequestMapping("/permiso/{usuarioId}")
    public List<CarpetaPermisoWrapper> permisoUsuario(Model model, @PathVariable("usuarioId") Long usuarioId) throws Exception {
        List<CarpetaPermiso> carpetaPermisoList = carpetaPermisoService.findAllByUsuario(usuarioId);
        List<CarpetaPermisoWrapper> cpwl = new ArrayList<>();
        if (carpetaPermisoList != null && carpetaPermisoList.size() > 0) {
            cpwl = carpetaPermisoList.stream().map(carpetaPermisoIndex).collect(Collectors.<CarpetaPermisoWrapper>toList());
        }
        return cpwl;
    }

   

}
