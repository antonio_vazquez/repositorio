package com.cie.repositorio.controller;

import com.cie.repositorio.form.NavForm;
import com.cie.repositorio.model.db.Carpeta;
import com.cie.repositorio.model.db.CarpetaPermiso;
import com.cie.repositorio.model.logic.CarpetaPermisoWrapper;
import com.cie.repositorio.model.logic.CarpetaWrapper;
import com.cie.repositorio.services.db.CarpetaPermisoService;
import com.cie.repositorio.services.db.CarpetaService;
import com.cie.repositorio.services.db.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/permiso")
public class PermisoController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private CarpetaService carpetaService;

    @Autowired
    private CarpetaPermisoService carpetaPermisoService;

    @RequestMapping("/permiso")
    public String index(Model model) throws Exception {
        model.addAttribute("form1", new NavForm());
        model.addAttribute("entities", usuarioService.findAll());
        return "permiso/permiso";
    }

    @RequestMapping("/usuario/{usuarioId}")
    public String index(Model model, @PathVariable("usuarioId") Long usuarioId) throws Exception {
        List<Carpeta> carpetaList = carpetaService.findAll().stream().filter(p->p.isEnable()).collect(Collectors.toList());
        List<CarpetaPermiso> carpetaPermisoList = carpetaPermisoService.findAllByUsuario(usuarioId);
        List<CarpetaPermisoWrapper> cpwl = new ArrayList<>();
        if (carpetaPermisoList != null && carpetaPermisoList.size() > 0) {
            cpwl = carpetaPermisoList.stream().map(carpetaPermisoIndex).collect(Collectors.<CarpetaPermisoWrapper>toList());
        }
        model.addAttribute("form1", new NavForm());
        model.addAttribute("usuario", usuarioService.findOne(usuarioId));
        model.addAttribute("carpeta", carpetaList.stream().map(carpetaIndex).collect(Collectors.<CarpetaWrapper>toList()));
        model.addAttribute("permiso", cpwl);
        return "permiso/usuario";
    }

    public static final Function<Carpeta, CarpetaWrapper> carpetaIndex = new Function<Carpeta, CarpetaWrapper>() {
        @Override
        public CarpetaWrapper apply(Carpeta c) {
            CarpetaWrapper cw = new CarpetaWrapper();
            cw.setId(c.getId());
            cw.setNombre(c.getNombre());
            cw.setDescripcion(c.getDescripcion());
            cw.setDirectory(c.isDirectory());
            cw.setIdParent(c.getIdParent());
            cw.setEnable(c.isEnable());
            return cw;
        }
    };

    public static final Function<CarpetaPermiso, CarpetaPermisoWrapper> carpetaPermisoIndex = new Function<CarpetaPermiso, CarpetaPermisoWrapper>() {
        @Override
        public CarpetaPermisoWrapper apply(CarpetaPermiso cp) {
            CarpetaPermisoWrapper cpw = new CarpetaPermisoWrapper();
            Carpeta c = new Carpeta();
            c.setId(cp.getCarpeta().getId());
            cpw.setId(cp.getId());
            cpw.setCarpeta(c);
            cpw.setPermiso(cp.getPermiso());
            return cpw;
        }
    };

}
