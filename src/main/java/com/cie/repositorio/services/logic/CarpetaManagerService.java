package com.cie.repositorio.services.logic;

import com.cie.repositorio.model.db.Carpeta;
import com.cie.repositorio.model.db.CarpetaPermiso;
import com.cie.repositorio.model.db.Tracking;
import com.cie.repositorio.model.logic.CarpetaForm;
import com.cie.repositorio.services.db.CarpetaPermisoService;
import com.cie.repositorio.services.db.CarpetaService;
import com.cie.repositorio.services.db.TrackingService;
import com.cie.repositorio.services.db.UsuarioService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.*;

@Service
public class CarpetaManagerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarpetaManagerService.class);

    @Autowired
    private CarpetaService carpetaService;

    @Autowired
    private TrackingService trackingService;

    @Autowired
    private CarpetaPermisoService carpetaPermisoService;

    @Autowired
    private UsuarioService usuarioService;

    public boolean insertCarpeta(CarpetaForm carpetaForm, BindingResult bindingResult, String usuario) {
        try {
            Carpeta carpeta = new Carpeta();
            Carpeta log = new Carpeta();
            if (!carpetaForm.isDirectorio() && carpetaForm.getArchivo().isEmpty()) {
                bindingResult.addError(new FieldError("form", "archivo", "Archivo requerido"));
                return false;
            }
            carpeta.setEnable(true);
            carpeta.setIdParent(carpetaForm.getParentId()); log.setIdParent(carpeta.getIdParent());
            carpeta.setDirectory(carpetaForm.isDirectorio()); log.setDirectory(carpeta.isDirectory());
            carpeta.setNombre(carpetaForm.getNombre()); log.setNombre(carpeta.getNombre());
            carpeta.setDescripcion(carpetaForm.getDescripcion()); log.setDescripcion(carpeta.getDescripcion());
            carpeta.setFechaCreacion( new Date()); log.setFechaCreacion(carpeta.getFechaCreacion());
            carpeta.setUsuarioCreacion(usuario); log.setUsuarioCreacion(carpeta.getUsuarioCreacion());
            if (!carpetaForm.isDirectorio() && !carpetaForm.getArchivo().isEmpty()) {
                carpeta.setArchivo(carpetaForm.getArchivo().getBytes());
                carpeta.setNombreArchivo(carpetaForm.getArchivo().getOriginalFilename()); log.setNombreArchivo(carpeta.getNombreArchivo());
                String[] pattern = carpetaForm.getArchivo().getOriginalFilename().split("\\.");
                carpeta.setExtension(pattern[pattern.length -1].toUpperCase()); log.setExtension(carpeta.getExtension());
            }
            Tracking tracking = new Tracking();
            tracking.setAction("INSERT");
            tracking.setRegister(new Date());
            tracking.setUser(usuario);
            tracking.setDirectory(carpeta.isDirectory());
            tracking.setNombre(carpeta.getNombre());
            log.setArchivo(null);
            Gson js = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
            tracking.setTrack(js.toJson(log,Carpeta.class));
            trackingService.insert(tracking);
            carpeta = carpetaService.insert(carpeta);
            List<CarpetaPermiso> carpetaPermisoList = carpetaPermisoService.findAllByParentId(carpeta.getIdParent());
            for (CarpetaPermiso c: carpetaPermisoList){
                CarpetaPermiso carpetaPermiso = new CarpetaPermiso();
                carpetaPermiso.setPermiso(c.getPermiso());
                carpetaPermiso.setCarpeta(carpeta);
                carpetaPermiso.setUsuario(c.getUsuario());
                carpetaPermisoService.insert(carpetaPermiso);
            }
/*            if (carpetaPermisoService.findAllByUsuarioAndCarpeta(usuarioService.findByCorreo(usuario).getId(),carpeta.getIdParent()).getPermiso() >1){
                CarpetaPermiso carpetaPermiso = new CarpetaPermiso();
                carpetaPermiso.setPermiso(2);
                carpetaPermiso.setCarpeta(carpeta);
                carpetaPermiso.setUsuario(usuarioService.findByCorreo(usuario));
                carpetaPermisoService.insert(carpetaPermiso);
            }*/
            return carpeta != null && carpeta.getId() > 0;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            bindingResult.addError(new FieldError("form", "nombre", "Error desconocido"));
            return false;
        }
    }

    private List<Carpeta> findParent(Long parentId) {
        Carpeta carpeta = carpetaService.findOne(parentId);
        if (carpeta.getIdParent() == 0) {
            List<Carpeta> cl = new ArrayList<>();
            cl.add(carpeta);
            return cl;
        } else {
            List<Carpeta> cl = new ArrayList<>();
            cl.add(carpeta);
            cl.addAll(findParent(carpeta.getIdParent()));
            return cl;
        }
    }

    public List<Carpeta> buildPath(Long parentId) {
        try {
            List<Carpeta> carpetaList = findParent(parentId);
            Collections.reverse(carpetaList);
            return carpetaList;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public String buildPathToString(Long parentId) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("Root");
            if (parentId != 0){
                for (Carpeta c : buildPath(parentId)) {
                    sb.append("/" + c.getNombre());
                }
            }
            return sb.toString();
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<Carpeta> findSons(Long parentId) {
        try {
            List<Carpeta> cl = carpetaService.findAllByParent(parentId);

            return cl;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<Carpeta> findSons(Long parentId, Long userId) {
        try {
            List<Carpeta> cl = carpetaService.findAllByParent(parentId);
            HashMap<Long, CarpetaPermiso> map = carpetaPermisoService.buildMapUsuario(userId);
            for (Carpeta carpeta : cl) {
                if (map.containsKey(carpeta.getId())) {
                    CarpetaPermiso cp = map.get(carpeta.getId());
                    carpeta.setPermiso(cp.getPermiso());
                } else {
                    carpeta.setPermiso(0);
                }
            }
            return cl;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<Carpeta> findIndex(List<Carpeta> cl, Long userId){
        try {
            HashMap<Long, CarpetaPermiso> map = carpetaPermisoService.buildMapUsuario(userId);
            for (Carpeta carpeta : cl) {
                if (map.containsKey(carpeta.getId())) {
                    CarpetaPermiso cp = map.get(carpeta.getId());
                    carpeta.setPermiso(cp.getPermiso());
                } else {
                    carpeta.setPermiso(0);
                }
            }
            return cl;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }
}
