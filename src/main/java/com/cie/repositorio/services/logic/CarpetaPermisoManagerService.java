package com.cie.repositorio.services.logic;

import com.cie.repositorio.model.db.Carpeta;
import com.cie.repositorio.model.db.CarpetaPermiso;
import com.cie.repositorio.services.db.CarpetaPermisoService;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarpetaPermisoManagerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarpetaPermisoManagerService.class);

    @Autowired
    private CarpetaPermisoService carpetaPermisoService;

    public List<CarpetaPermiso> insertCarpeta(List<CarpetaPermiso> carpetaPermisoList) {
        try {
            List<CarpetaPermiso> toSave = carpetaPermisoList.stream().filter(p->p.getId() < 1).collect(Collectors.toList());
            carpetaPermisoService.insert(toSave);
            for (CarpetaPermiso cp : carpetaPermisoList.stream().filter(p->p.getId() > 0).collect(Collectors.toList())) {
                carpetaPermisoService.update(cp);
            }
            return carpetaPermisoService.findAllByUsuario(carpetaPermisoList.get(0).getUsuario().getId());
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }
}
