package com.cie.repositorio.services.db;

import com.cie.repositorio.model.db.*;
import com.cie.repositorio.model.logic.CarpetaForm;
import com.cie.repositorio.repository.CarpetaRepository;
import com.cie.repositorio.repository.UsuarioRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.querydsl.core.BooleanBuilder;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.FieldError;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CarpetaService extends AbstractService<Carpeta, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarpetaService.class);

    @Autowired
    private CarpetaRepository repository;

    @Autowired
    private CarpetaService carpetaService;

    @Autowired TrackingService trackingService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    public Carpeta update(CarpetaForm r){
        Carpeta toUpdate = new Carpeta();
        Carpeta log = new Carpeta();
        try{
            toUpdate= carpetaService.findOne(r.getId()); log =carpetaService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre()); log.setNombre(toUpdate.getNombre());
            toUpdate.setDescripcion(r.getDescripcion()); log.setDescripcion(toUpdate.getDescripcion());
            toUpdate.setFechaActualizacion(new Date()); log.setFechaActualizacion(toUpdate.getFechaActualizacion());
            toUpdate.setUsuarioActualizacion(r.getUsuarioActualizacion()); log.setUsuarioActualizacion(toUpdate.getUsuarioActualizacion());
            if (r.getArchivo() != null && r.getArchivo().getSize() > 0){
                toUpdate.setArchivo(r.getArchivo().getBytes());
                toUpdate.setNombreArchivo(r.getArchivo().getOriginalFilename());log.setNombreArchivo(toUpdate.getNombreArchivo());
                String[] pattern = r.getArchivo().getOriginalFilename().split("\\.");
                toUpdate.setExtension(pattern[pattern.length -1].toUpperCase());log.setExtension(toUpdate.getExtension());
            }

            Tracking tracking = new Tracking();
            tracking.setAction("UPDATE");
            tracking.setRegister(new Date());
            tracking.setUser(toUpdate.getUsuarioActualizacion());
            tracking.setDirectory(toUpdate.isDirectory());
            tracking.setNombre(toUpdate.getNombre());
            Gson js = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
            tracking.setTrack(js.toJson(toUpdate, Carpeta.class));

            trackingService.insert(tracking);

        }catch (Exception e){
            LOGGER.error("Ocurrio un error :[" + ExceptionUtils.getFullStackTrace(e)+"]");
            return null;
        }
        return toUpdate;
    }

    public Carpeta disableFile(Long id){
        Carpeta toUpdate = new Carpeta();
        SimpleDateFormat formateador = new SimpleDateFormat("ddMMyyyy_hhmm");

        try{
            toUpdate= carpetaService.findOne(id);
            toUpdate.setNombre(carpetaService.findOne(id).getNombre()+"_"+formateador.format(new Date()));
            toUpdate.setEnable(false);

        }catch (Exception e){
            LOGGER.error("Ocurrio un error: ["+ ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public List<Carpeta> findAllByParent(Long idParent) {
        try {
            return repository.findAllByIdParent(idParent);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<Carpeta> queryBuilder(String nombre) {
        try {
            QCarpeta carpeta = QCarpeta.carpeta;
            BooleanBuilder where = new BooleanBuilder();
            if (!StringUtils.isEmpty(nombre)) {
                where.and(carpeta.nombre.contains(nombre));
            }
            return IteratorUtils.toList(this.repository.findAll(where.getValue()).iterator());
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }
}
