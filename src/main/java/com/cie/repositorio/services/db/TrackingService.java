package com.cie.repositorio.services.db;

import com.cie.repositorio.model.db.QTracking;
import com.cie.repositorio.model.db.Rol;
import com.cie.repositorio.model.db.Tracking;
import com.cie.repositorio.repository.RolRepository;
import com.cie.repositorio.repository.TrackingRepository;
import com.querydsl.core.BooleanBuilder;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TrackingService extends AbstractService<Tracking, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrackingService.class);

    @Autowired
    private TrackingRepository repository;

    @Autowired
    private TrackingService trackingService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Tracking update(Tracking r) {
        Tracking toUpdate = new Tracking();
        try {
            toUpdate =  trackingService.findOne(r.getId());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public List<Tracking> queryBuilder(Date desde, Date hasta, String nombre, String usuario) {
        try {
            QTracking tracking = QTracking.tracking;
            BooleanBuilder where = new BooleanBuilder();
                if (desde != null) {
                    where.and(tracking.register.after(desde));
                }
                if (hasta != null) {
                    where.and(tracking.register.before(hasta));
                }
            if (!StringUtils.isEmpty(nombre)) {
                where.and(tracking.nombre.contains(nombre));
            }
            if (!StringUtils.isEmpty(usuario)) {
                where.and(tracking.user.contains(usuario));
            }
            return IteratorUtils.toList(this.repository.findAll(where.getValue()).iterator());
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }


}
