package com.cie.repositorio.services.db;

import com.cie.repositorio.model.db.Carpeta;
import com.cie.repositorio.model.db.CarpetaPermiso;
import com.cie.repositorio.model.db.QCarpeta;
import com.cie.repositorio.model.db.Tracking;
import com.cie.repositorio.model.logic.CarpetaForm;
import com.cie.repositorio.repository.CarpetaPermisoRepository;
import com.cie.repositorio.repository.CarpetaRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.querydsl.core.BooleanBuilder;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
@Transactional
public class CarpetaPermisoService extends AbstractService<CarpetaPermiso, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarpetaPermisoService.class);

    @Autowired
    private CarpetaPermisoRepository repository;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    public List<CarpetaPermiso> findAllByUsuario(long usuarioId) {
        try {
            return repository.findAllByUsuarioId(usuarioId);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public CarpetaPermiso findAllByUsuarioAndCarpeta(long usuarioId, long carpetaId) {
        try {
            return repository.findAllByUsuarioIdAndCarpetaId(usuarioId, carpetaId);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<CarpetaPermiso> findAllByParentId(long parentId){
        try {
            return repository.findAllByCarpetaId(parentId);
        }catch (Exception e){
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public HashMap<Long, CarpetaPermiso> buildMapUsuario(long usuarioId) {
        try {
            HashMap<Long, CarpetaPermiso> map = new HashMap<>();
            for (CarpetaPermiso carpetaPermiso : this.findAllByUsuario(usuarioId)) {
                map.put(carpetaPermiso.getCarpeta().getId(), carpetaPermiso);
            }
            return map;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<CarpetaPermiso> findAllByCarpertaId(long carpetaId){
        try{
            return repository.findAllByCarpetaId(carpetaId);
        } catch (Exception e){
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

}
