package com.cie.repositorio.services.db;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public abstract class AbstractService<T, ID extends Serializable> {

    JpaRepository<T, ID> repository;

    Logger LOGGER;

    public JpaRepository<T, ID> getRepository() {
        return repository;
    }

    public void setRepository(JpaRepository<T, ID> repository) {
        this.repository = repository;
    }

    public Logger getLOGGER() {
        return LOGGER;
    }

    public void setLOGGER(Logger LOGGER) {
        this.LOGGER = LOGGER;
    }

    public T insert(T entity) {
        try {
            if (propertyExists(entity, "registro")) {
                PropertyUtils.setProperty(entity, "registro", new Date());
            }
            T toCreate = repository.save(entity);
            return toCreate;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public void insert(List<T> entities) {
        try {
            if (entities != null && entities.size() >0) {
                for (T t : entities) {
                    if (propertyExists(t, "registro")) {
                        PropertyUtils.setProperty(t, "registro", new Date());
                    }
                }
            }
            Iterable<T> iterable = entities;
            repository.saveAll(iterable);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
    }

    public T findOne(ID id) {
        try {
            T found = repository.findById(id).get();
            return found;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<T> findAll() {
        try {
            List<T> list = repository.findAll();
            return list;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    @Transactional
    public T update(T entity) {
        try {
            ID id = (ID) PropertyUtils.getProperty(entity, "id");
            T found = repository.findById(id).get();
            if (propertyExists(entity, "registro")) {
                PropertyUtils.setProperty(entity, "registro", PropertyUtils.getProperty(found, "registro"));
            }
            if (propertyExists(entity, "createdDate")) {
                PropertyUtils.setProperty(entity, "createdDate", PropertyUtils.getProperty(found, "createdDate"));
            }
            if (propertyExists(entity, "createdBy")) {
                PropertyUtils.setProperty(entity, "createdBy", PropertyUtils.getProperty(found, "createdBy"));
            }
            PropertyUtils.copyProperties(found, entity);
            return found;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public T delete(T entity) {
        try {
            repository.delete(entity);
            return entity;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public T delete(ID id) {
        try {
            T toDelete = repository.findById(id).get();
            repository.delete(toDelete);
            return toDelete;
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public void delete(List<T> l)  {
        try {
            Iterable<T> iterable = l;
            repository.deleteAll(iterable);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
    }

    public Page<T> findPaginated(Pageable pageable) {
        try {
            return repository.findAll(pageable);
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    private boolean propertyExists(T bean, String property) {
        return PropertyUtils.isReadable(bean, property) &&
                PropertyUtils.isWriteable(bean, property);
    }

}