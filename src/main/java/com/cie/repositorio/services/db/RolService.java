package com.cie.repositorio.services.db;

import com.cie.repositorio.model.db.Rol;
import com.cie.repositorio.repository.RolRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class RolService extends AbstractService<Rol, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RolService.class);

    @Autowired
    private RolRepository repository;

    @Autowired
    private RolService rolService;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Rol update(Rol r) {
        Rol toUpdate = new Rol();
        try {
            toUpdate =  rolService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }



}
