package com.cie.repositorio.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class TrackingForm {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date desde;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date hasta;

    private String nombre;

    private String usuario;

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
